import java.util.Scanner;

public class LB_1 {

    private static double PI = 3.14;

    public static void main(String [] args)
    {
        // 12.56 = 12.56
        CircleSquare();
        // 25 = 10
        CircleLength();
        // 1234 = 10
        Summ();
        // 2, 2 = 4, 2
        SquareAndMod();
    }

    /*
        Дана довжина кола. Знайти площу круга, обмеженого
        цим колом. Вважати значення Pi рівним 3.14.
    */
    public static void CircleSquare()
    {
        System.out.println("Введите длину круга");
        Scanner input = new Scanner(System.in);
        double length = input.nextDouble(),
                radius = length / (2*PI),
                square = PI * Math.pow( radius, 2);

        System.out.println("Circle square: " + square);
    }

    /*
        Дана площа круга. Знайти довжину кола, що обмежує
        цей круг. Вважати значення Pi рівним 3.14.
    */
    public static void CircleLength()
    {
        System.out.println("Введите площадь круга");
        Scanner input = new Scanner(System.in);
        double square = input.nextDouble(),
                radius = Math.sqrt(square)/PI,
                length = 2*PI*radius;

        System.out.println("Circle length: " + length);
    }

    /*
        Дано ціле чотиризначне число. Використовуючи
        операції цілочисленого ділення та остачу , знайти
        суму його цифр.
    */
    public static void Summ()
    {
        System.out.println("Введите четырех значное число");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt(),
                num1 = num % 10,
                num2 = (num / 10) % 10,
                num3 = (num / 100) % 10,
                num4 = num / 1000,

                sum = num1 + num2 + num3 + num4;
        System.out.println("The sum is: " + sum);
    }

    /*
     Дані два числа. Знайти середнє арифметичне їх
     квадратів і середнє арифметичне їх модулів.
     */
    public static void SquareAndMod()
    {
        System.out.println("Введите число №1");
        Scanner input = new Scanner(System.in);
        int num1 = input.nextInt();

        System.out.println("Введите число №2");
        Scanner input2 = new Scanner(System.in);
        int num2 = input2.nextInt();

        double squares = ( Math.pow(num1, 2) + Math.pow(num2, 2) ) / 2;
        int mods = (Math.abs(num1) + Math.abs(num2)) / 2;
        System.out.printf("САК: %.2f, САМ: %d", squares, mods);
    }
}