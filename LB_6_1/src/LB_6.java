public class LB_6 {

    static ObjectClass[] data = new ObjectClass [8];

    public static void main(String[] args) {

        for(int i = 0; i < 8; i++)
        {
            data[i] = new ObjectClass(Long.toHexString(Double.doubleToLongBits(Math.random())));
        }

        try {
            for(int j = 0; j < 10; j++) {
                System.out.println("Index: " + j + " | data: "  + data[j].name);
            }
        }
        catch(NullPointerException e) {
            System.out.println("Масив порожній.");
        }
        catch(IndexOutOfBoundsException e) {
            System.out.println("Здається, масив закінчився.");
        }

        try {
            for(int i = 0; i < 8; i++) {
                System.out.println("Index: " + i + " | data: "  + data[i].name);

                if(i == 2) {
                    throw new FatalException();
                }
            }
        }
        catch(FatalException e)
        {
            System.out.println(e.getMessage());
        }
    }
}

class ObjectClass{
    String name;

    public ObjectClass (String name){
        this.name = name;
    }
}
