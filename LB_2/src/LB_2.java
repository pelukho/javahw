import org.jetbrains.annotations.NotNull;
import java.util.Scanner;

public class LB_2 {
    public static void main(String[] args) {
        Task_1_1();
        Task_1_2();
        Task_2_1();
        Task_2_2();
        Task_3_1();
        Task_3_2();
    }

    @NotNull
    public static int[] Get_Number()
    {
        System.out.println("Введите трехзначное число");
        Scanner input = new Scanner(System.in);
        int[] number = new int[3];

        int num = input.nextInt();
        number[0] = num / 100;
        number[1] = (num / 10) % 10;
        number[2] = num % 10;

        return number;
    }

    public static void Task_1_1()
    {
        int[] num = Get_Number();

        if ((num[0] != num[1]) && (num[1] != num[2]) && (num[0] != num[2]))
            System.out.println("Все числа данного трехзначного числа разные.");
        else
            System.out.println("Одно или больше чисел одинаковые");
    }

    public static void Task_1_2()
    {
        System.out.println("Variant 1, task 2: ");
        for(int i = 1; i <= 512; i *= 2 ) System.out.println(i);
    }

    public static void Task_2_1()
    {
        int[] num = Get_Number();

        if((num[0] < num[1]) && (num[1] < num[2]))
            System.out.println("Числа данного трехзначного числа образуют возрастающую последовательность. ");
        else
            System.out.println("Числа данного трехзначного числа не образуют возрастающую последовательность. ");
    }

    public static void Task_2_2()
    {
        System.out.println("Variant 2, task 2: ");
        int i = 1;
        while(i <= 512 )
        {
            System.out.println(i);
            i *= 2;
        }
    }

    public static void Task_3_1()
    {
        int[] num = Get_Number();
        int sum = num[0] + num[1] + num[2];

        if(0 == (sum % 2))
            System.out.println("Сумма цифр данного трехзначного числа является четным числом");
        else
            System.out.println("Сумма цифр данного трехзначного числа не является четным числом");
    }

    public static void Task_3_2()
    {
        int i = 1;
        System.out.println("Variant 3, task 2: ");
        do
        {
            System.out.println(i);
            i *= 2;
        } while(i <= 512);
    }
}
