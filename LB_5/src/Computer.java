public class Computer extends AbstractComputer {

    private static int compCount = 0;
    private String compType;

    public Computer(
            String compType,
            String cpu,
            String motherboard,
            String videocard,
            int ram,
            String powerSupply,
            int usb,
            String monitor,
            String keyboard,
            String mouse
    ) {
        super(cpu, motherboard, videocard, ram, powerSupply, usb, monitor, keyboard, mouse);
        this.compType = compType;
        compCount++;
    }

    @Override
    void print() {
        System.out.printf(
                "Computer type: %s \n" +
                "Computer processor: %s \n" +
                "Computer motherboard: %s \n" +
                "Computer videocard: %s \n" +
                "Computer RAM: %d \n" +
                "Power supply: %s \n" +
                "Counts of USB ports: %d \n" +
                "Monitor: %s \n" +
                "Keyboard: %s \n" +
                "Mouse: %s \n",
                compType, cpu, motherboard, videocard, ram, powerSupply, usb, monitor, keyboard, mouse
        );
    }

    public void GetCompCount()
    {
        System.out.println("Computers created: " + compCount);
    }
}
