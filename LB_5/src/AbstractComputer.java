public abstract class AbstractComputer {
    protected String cpu;
    protected String motherboard;
    protected String videocard;
    protected int ram;
    protected String powerSupply;
    protected int usb;
    protected String monitor;
    protected String keyboard;
    protected String mouse;

    public AbstractComputer(
            String cpu,
            String motherboard,
            String videocard,
            int ram,
            String powerSupply,
            int usb,
            String monitor,
            String keyboard,
            String mouse

    ) {
        this.cpu = cpu;
        this.motherboard = motherboard;
        this.videocard = videocard;
        this.ram = ram;
        this.powerSupply = powerSupply;
        this.usb = usb;
        this.monitor = monitor;
        this.keyboard = keyboard;
        this.mouse = mouse;
    }

    abstract void print();
}
