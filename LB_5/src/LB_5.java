class LB_5 {
    public static void main(String[] args) {
        Computer compForGames = new Computer(
                "Computer for games",
                "Celeron cor 2 duo",
                "MX2314",
                "GForce M2314",
                16384,
                "Super power supply",
                5,
                "Samsung",
                "Xiomi",
                "razer v5"
        );

        Computer compForOffice = new Computer(
                "Computer for office",
                "Celeron cor2duo",
                "MT1214",
                "GForce G2",
                8192,
                "Power supply",
                2,
                "Lenovo",
                "Zend",
                "razer v1"
        );

        Computer compForHome = new Computer(
                "Computer for home",
                "Intel Pentium 4",
                "PZ31",
                "GForce 15",
                1024,
                "Power supply",
                2,
                "Lenovo",
                "Zend",
                "razer v1"
        );

        compForGames.print();
        System.out.println("------------------------------------");

        compForOffice.print();
        System.out.println("------------------------------------");

        compForHome.print();
        System.out.println("------------------------------------");

        compForGames.GetCompCount();
    }
}
