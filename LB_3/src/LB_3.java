import java.util.Scanner;

public class LB_3 {
    public static void main(String[] args) {
        Task_1();
        Task_2();
        Task_3();
        Task_4();
    }

    public static void Task_1()
    {
        int min, max;
        String min_pos = "", max_pos = "";
        int[][] matrix = {
                {4, 5, 6, 7},
                {8, 9, 8, 7},
                {7, 5, 4 ,3}
        };

        min = matrix[0][0];
        max = matrix[0][0];

        for(int i = 0; i < matrix.length; i++)
        {
            for(int j = 0; j < matrix[i].length; j++)
            {
                if(matrix[i][j] < min)
                {
                    min = matrix[i][j];
                    min_pos = "matrix[" + i + "][" + j + "]";
                }

                if(matrix[i][j] > max)
                {
                    max = matrix[i][j];
                    max_pos = "matrix[" + i + "][" + j + "]";
                }
            }
        }

        System.out.printf(
                "Min number: %d, position: %s;\n" +
                        "Max number: %d, position: %s\n",
                min,
                min_pos,
                max,
                max_pos
        );
    }

    public static void Task_2()
    {
        int counter = 0;
        int[][] matrix = {
                {0, 1, 1, 0},
                {1, 0, 1, 0},
                {1, 1, 0 ,1}
        };

        for(int i = 0; i < matrix.length; i++)
        {
            for(int j = 0; j < matrix[i].length; j++)
            {
                if(0 == matrix[i][j])
                    counter++;
            }
        }

        System.out.printf("Количество нулей в массиве %d\n", counter);
    }

    public static void Task_3()
    {
        int counter = 0;
        int[][] matrix = {
                {4, 5, 6, 7},
                {8, 9, 8, 7},
                {7, 5, 4 ,3}
        };
        System.out.println("Введите число для поиска по массиву\n");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();

        for(int i = 0; i < matrix.length; i++)
        {
            for(int j = 0; j < matrix[i].length; j++)
            {
                if(number == matrix[i][j])
                    counter++;
            }
        }

        if(0 == counter)
            System.out.println("Данное число в массиве не найдено\n");
        else
            System.out.printf("Число %d найдено в массиве %d раз(a)\n", number, counter);
    }

    public static void Task_4()
    {
        int[][] matrix = new int[3][3];

        for(int i  = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                matrix[i][j] = i + j;

                System.out.print(matrix[i][j] + "\t");
                if(2 == j)
                    System.out.print("\n");
            }
        }

    }
}
