import org.jetbrains.annotations.NotNull;

public class LB_4 {
    public static void main(String[] args) {
        Vector3D vector1 = new Vector3D(15, 54.8, -32);
        Vector3D vector2 = new Vector3D(63, -5, 41);
        System.out.format("A vector's axis is %s.\n", vector1);
        System.out.format("B vector's axis is %s.\n", vector2);
        System.out.format("A vector's length is %s.\n", vector1.getModule());
        System.out.format("B vector's length is %s.\n", vector2.getModule());
        System.out.format("The sum of these vectors is %s.\n", vector1.getAddition(vector2));
        System.out.format("The subtraction of these vectors is %s.\n", vector1.getSubtraction(vector2));
        System.out.format("The scalar product of these vectors is %s.\n", vector1.getScalar(vector2));
        System.out.format("The vector product of these vectors is %s.\n", vector1.getVector(vector2));
    }
}

class Vector3D {
    private double x, y, z;

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getModule() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public Vector3D getAddition(@NotNull Vector3D vector) {
        return new Vector3D(x + vector.x, y + vector.y, z + vector.z);
    }

    public Vector3D getSubtraction(@NotNull Vector3D vector) {
        return new Vector3D(x - vector.x, y - vector.y, z - vector.z);
    }

    public double getScalar(@NotNull Vector3D vector) {
        return x * vector.x + y * vector.y + z * vector.z;
    }

    public Vector3D getVector(@NotNull Vector3D vector) {
        return new Vector3D(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x);
    }

    @Override
    public String toString() {
        return "{" + x + "; " + y + "; " + z + "}";
    }

}