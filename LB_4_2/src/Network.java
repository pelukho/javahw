import java.util.ArrayList;

public class Network {

    private static Network instance;
    public ArrayList <String> numberList = new ArrayList<>();;

    private Network() {
        this.numberList.add("+380631231212");
        this.numberList.add("+380635342512");
        this.numberList.add("+380631111111");
        this.numberList.add("+380635603201");
    }

    public static Network getInstance() {
        if (null == instance) {
            instance = new Network();
        }
        return instance;
    }

    public void AddNumberToNetwork(String number)
    {
        this.numberList.add(number);
    }

    public boolean FindNumberInNetwork(String number)
    {
        return this.numberList.contains(number);
    }
}
