public class LB_4_2 {
    public static void main(String[] args) {
        Phone phone = new Phone("+380734022519");
        phone.RegistrationInNetwork();

        phone.call("+380631231212"); // successful call
        phone.call("+380734022511"); // unsuccessful call
        phone.call("+380734022519"); // call to own number
    }
}
