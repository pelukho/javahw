public class Phone {
    private String number;
    private Network network;

    public Phone(String number)
    {
        this.number = number;
        this.network = Network.getInstance();
    }

    public void RegistrationInNetwork()
    {
        network.AddNumberToNetwork(this.number);
    }

    public void call(String callNumber)
    {
        boolean isCalled = network.FindNumberInNetwork(callNumber);

        if(callNumber == this.number) {
            System.out.println("Unable to call for own number");
        } else if(isCalled) {
            System.out.printf("Calling to number %s\n", callNumber);
        } else {
            System.out.printf("Number %s not found in network\n", callNumber);
        }
    }
}
